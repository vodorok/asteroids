#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_ttf.h>

#include <stdlib.h>

#define HEIGHT 600
#define WIDTH 800
#define PI 3.14159265
#define AP 11

//#define DEBUG


typedef enum{
    SPACESHIP,
    ASTEROID,
    PROJECTILE,
    PARTICE
} EntityType;

typedef enum{
    LARGE = 50,
    MEDIUM = 20,
    TINY = 10,
} AsteroidSize;

typedef enum{
    MENU,
    INGAME,
    SCORE,
} GameState;

typedef struct{
    float x;
    float y;
} Position;

typedef struct{
    float x;
    float y;
    float acc;
} Velocity;

typedef struct{
    Position pos;
    float orient;
    Velocity vel;
    int id;
    int life;
} Common;

typedef struct{
    Common com;
    float surfacJag[AP];
    AsteroidSize size;
} Asteroid;

typedef struct{
    Common com;
    // for better collision handling
    Position position[4];
} SpaceShip;

typedef struct{
    Common com;
} Projectile;

typedef union {
    Asteroid a;
    SpaceShip s;
    Projectile p;
} SubType;

typedef struct _Entity {
    EntityType type;
    SubType subtype;
    struct _Entity *nextListItem;
    struct _Entity *prevListItem;
} Entity;

typedef struct _EntityLists{
    Entity firstAsSentinel, lastAsSentinel;
    Entity firstPrSentinel, lastPrSentinel;
    Entity firstSsSentinel, lastSsSentinel;
} EntityLists;

typedef struct{
    int level;
    int points;
    GameState state;
} Context;

SDL_Surface *screen;
SDL_Event event;
TTF_Font *font;

void Init(EntityLists *entityList, Context *context);
void SdlInit();
void RunGame(EntityLists *entityList, Context *context);
void GameLoop(EntityLists *entityList, Context *context);
void ManageEntities(EntityLists *entityList, Context *context);
void MoveEntities(EntityLists *entityList);
void ReDraw(EntityLists *entityList, Context *context);
void InputHandler(EntityLists *entityList, Context *context);

void ChangeVelocites(Entity *spaceShipList);
void DetectCollision(EntityLists *entityList, Context *context);
void Shoot(Entity *projectileList, Entity *spaceShipList);

void PopulateAsteroidList(Entity *asteroidList, int n, AsteroidSize size, int posX, int posY);
void PopulateSpaceShipList(Entity *spaceShipList);
void AddToList(Entity *item, Entity *list);
void GarbageCollect(EntityLists *entityList);

void EntityHandler(EntityLists *entityList, Context *context);

void GetRotatedSpaceshipPoints(Entity *spaceShipList, Sint16 *posX, Sint16 *posY);
void DrawSpaceShip(Entity *spaceShipList);
void DrawAsteroid(Entity *asteroidList);
void DrawProjectile(Entity *projectileList);

Entity *NewAsteroid(AsteroidSize size, int posX, int posY);
Entity *NewProjectile(Entity *spaceShipList);
Entity *NewSpaceship();

float GetFloatRand();
bool isListEmpty(Entity *list);

void SdlInit(){
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    screen = SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_ANYFORMAT|SDL_DOUBLEBUF);
    if (!screen) {
        fprintf(stderr, "Cannot open window!\n");
        exit(1);
    }
    SDL_WM_SetCaption("Asteroids", "Asteroids");

    TTF_Init();
    font = TTF_OpenFont("LiberationMono-Regular.ttf", 20);
    if (!font) {
        fprintf(stderr, "nincs betu!\n");
    }
}

int GetSafeSpawn(int x){
    int spot = rand() % (x+1);
    if(x - spot < x/2+100 && x - spot > x/2-100){
        GetSafeSpawn(x);
    }
}

Entity *NewAsteroid(AsteroidSize size, int posX, int posY){
    Entity *newAsteroid = (Entity*)malloc(sizeof(Entity));
    newAsteroid->type = ASTEROID;
    newAsteroid->subtype.a.com.life = 1;
    newAsteroid->subtype.a.com.orient = 0.0f;

    if(posX == 0){
        newAsteroid->subtype.a.com.pos.x = GetSafeSpawn(WIDTH);
    } else {
        newAsteroid->subtype.a.com.pos.x = posX;
    }
    if(posY == 0){
        newAsteroid->subtype.a.com.pos.y = GetSafeSpawn(HEIGHT);
    } else {
        newAsteroid->subtype.a.com.pos.y = posY;
    }

    newAsteroid->subtype.a.com.vel.x = GetFloatRand();
    newAsteroid->subtype.a.com.vel.y = GetFloatRand();

    newAsteroid->subtype.a.com.vel.acc = 0.0f;

    newAsteroid->subtype.a.size = size;

    for(int i = 0; i < AP; i++){
         newAsteroid->subtype.a.surfacJag[i] = GetFloatRand();
    }
    return newAsteroid;
}

Entity *NewProjectile(Entity *spaceShipList){
    float posX, posY, velX, velY, orient;
    for(spaceShipList=spaceShipList->nextListItem;
        spaceShipList->nextListItem != NULL;
        spaceShipList=spaceShipList->nextListItem){
        if(spaceShipList->type == SPACESHIP){
            posX = spaceShipList->subtype.s.com.pos.x;
            posY = spaceShipList->subtype.s.com.pos.y;

            velX = spaceShipList->subtype.s.com.vel.x;
            velY = spaceShipList->subtype.s.com.vel.y;
            orient = spaceShipList->subtype.s.com.orient;
        }
    }

    Entity *newProjectile = (Entity*)malloc(sizeof(Entity));
    newProjectile->type = PROJECTILE;
    newProjectile->subtype.p.com.life = 60;
    newProjectile->subtype.p.com.orient = 0.0f;

    newProjectile->subtype.p.com.pos.x = posX;
    newProjectile->subtype.p.com.pos.y = posY;

    newProjectile->subtype.p.com.vel.x = velX+sin(-orient+PI/2)*10;
    newProjectile->subtype.p.com.vel.y = velY+cos(-orient+PI/2)*10;

    newProjectile->subtype.p.com.vel.acc = 0.0f;
    return newProjectile;
}

Entity *NewSpaceship(){
    Entity *newSpaceship = (Entity*)malloc(sizeof(Entity));
    newSpaceship->type = SPACESHIP;
    newSpaceship->subtype.s.com.life = 1;
    newSpaceship->subtype.s.com.orient = 0.0f;

    newSpaceship->subtype.s.com.pos.x = WIDTH/2;
    newSpaceship->subtype.s.com.pos.y = HEIGHT/2;

    newSpaceship->subtype.s.com.vel.x = 0;
    newSpaceship->subtype.s.com.vel.y = 0;

    newSpaceship->subtype.s.com.vel.acc = 0.075f;
    return newSpaceship;
}

void Shoot(Entity *projectileList, Entity *spaceShipList){
    Entity *projectile = NewProjectile(spaceShipList);
    AddToList(projectile, projectileList);
}

void AddToList(Entity *item, Entity *list){
    item->prevListItem = list;
    item->nextListItem = list->nextListItem;

    list->nextListItem->prevListItem = item;
    list->nextListItem = item;
}

void RemoveFromList(Entity *item, Entity *list){
    item->prevListItem = list;
    item->nextListItem = list->nextListItem;

    list->nextListItem->prevListItem = item;
    list->nextListItem = item;
}

float GetFloatRand(){
    float a = 2.0;
    float i = (((float)rand()/(float)(RAND_MAX)) * a)-1;
    return i;
}

void PopulateAsteroidList(Entity *asteroidList, int n, AsteroidSize size, int posX, int posY){
    //at first only lARGE asteroids exists
    for(int i = 1; i <= n; i++){
            Entity *asteroid = NewAsteroid(size, posX, posY);
            AddToList(asteroid, asteroidList);
    }
}

void PopulateSpaceShipList(Entity *spaceShipList){
    Entity *spaceShip = NewSpaceship();
    AddToList(spaceShip, spaceShipList);
}

void EntityHandler(EntityLists *entityList, Context *context){
    Entity *asteroidList = &(entityList->firstAsSentinel);
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    PopulateAsteroidList(asteroidList, context->level, LARGE, 0, 0);
    PopulateSpaceShipList(spaceShipList);
}

void Init(EntityLists *entityList, Context *context){
    srand((unsigned int)time(NULL));

    EntityHandler(entityList, context);
    SdlInit();
    RunGame(entityList, context);
}

void MoveEntities(EntityLists *entityList){
    Entity *asteroidList = &(entityList->firstAsSentinel);

    for(asteroidList=asteroidList->nextListItem;
        asteroidList->nextListItem != NULL;
        asteroidList=asteroidList->nextListItem){
        if(asteroidList->subtype.a.com.life){
            asteroidList->subtype.a.com.pos.x += asteroidList->subtype.a.com.vel.x;
            if(asteroidList->subtype.a.com.pos.x >= WIDTH){
                asteroidList->subtype.a.com.pos.x = 1;
            } else if(asteroidList->subtype.a.com.pos.x <= 0){
                asteroidList->subtype.a.com.pos.x = WIDTH;
            }
            asteroidList->subtype.a.com.pos.y += asteroidList->subtype.a.com.vel.y;
            if(asteroidList->subtype.a.com.pos.y >= HEIGHT){
                asteroidList->subtype.a.com.pos.y = 1;
            } else if(asteroidList->subtype.a.com.pos.y <= 0){
                asteroidList->subtype.a.com.pos.y = HEIGHT;
            }
        }
    }
    Entity *projectileList = &(entityList->firstPrSentinel);

    for(projectileList=projectileList->nextListItem;
        projectileList->nextListItem != NULL;
        projectileList=projectileList->nextListItem){
        if(projectileList->subtype.p.com.life){
            projectileList->subtype.p.com.pos.x += projectileList->subtype.p.com.vel.x;
            if(projectileList->subtype.p.com.pos.x >= WIDTH){
                projectileList->subtype.p.com.pos.x = 1;
            } else if(projectileList->subtype.p.com.pos.x <= 0){
                projectileList->subtype.p.com.pos.x = WIDTH;
            }
            projectileList->subtype.p.com.pos.y += projectileList->subtype.p.com.vel.y;
            if(projectileList->subtype.p.com.pos.y >= HEIGHT){
                projectileList->subtype.p.com.pos.y = 1;
            } else if(projectileList->subtype.p.com.pos.y <= 0){
                projectileList->subtype.p.com.pos.y = HEIGHT;
            }
        }
    }
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    for(spaceShipList=spaceShipList->nextListItem;
        spaceShipList->nextListItem != NULL;
        spaceShipList=spaceShipList->nextListItem){
        if(spaceShipList->subtype.s.com.life){
            spaceShipList->subtype.s.com.pos.x += spaceShipList->subtype.s.com.vel.x;
            if(spaceShipList->subtype.s.com.pos.x >= WIDTH){
                spaceShipList->subtype.s.com.pos.x = 1;
            } else if(spaceShipList->subtype.s.com.pos.x <= 0){
                spaceShipList->subtype.s.com.pos.x = WIDTH;
            }
            spaceShipList->subtype.s.com.pos.y += spaceShipList->subtype.s.com.vel.y;
            if(spaceShipList->subtype.s.com.pos.y >= HEIGHT){
                spaceShipList->subtype.s.com.pos.y = 1;
            } else if(spaceShipList->subtype.s.com.pos.y <= 0){
                spaceShipList->subtype.s.com.pos.y = HEIGHT;
            }
        }
    }
}

void DetectCollision(EntityLists *entityList, Context *context){
    float critDist;
    Entity *asteroidList = &(entityList->firstAsSentinel);
    Entity *projectileList = &(entityList->firstPrSentinel);
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    for(asteroidList=asteroidList->nextListItem;
        asteroidList->nextListItem != NULL;
        asteroidList=asteroidList->nextListItem){

        Entity *tempList1 = projectileList, *tempList2 = spaceShipList;
        for(tempList1=tempList1->nextListItem;
            tempList1->nextListItem != NULL;
            tempList1=tempList1->nextListItem){
            critDist = (tempList1->subtype.p.com.pos.x-asteroidList->subtype.a.com.pos.x) *
                            (tempList1->subtype.p.com.pos.x-asteroidList->subtype.a.com.pos.x) +
                            (tempList1->subtype.p.com.pos.y-asteroidList->subtype.a.com.pos.y) *
                            (tempList1->subtype.p.com.pos.y-asteroidList->subtype.a.com.pos.y);
            if(critDist<=(2+asteroidList->subtype.a.size)*(2+asteroidList->subtype.a.size)){
                tempList1->subtype.p.com.life = 0;
                asteroidList->subtype.a.com.life = 0;
                context->points++;
                if(asteroidList->subtype.a.size == LARGE){
                    PopulateAsteroidList(asteroidList, 3, MEDIUM,
                                         asteroidList->subtype.a.com.pos.x,
                                         asteroidList->subtype.a.com.pos.y);
                    break;
                } else if(asteroidList->subtype.a.size == MEDIUM){
                    PopulateAsteroidList(asteroidList, 3, TINY,
                                         asteroidList->subtype.a.com.pos.x,
                                         asteroidList->subtype.a.com.pos.y);
                    break;
                }
            }
        }
        for(tempList2=tempList2->nextListItem;
            tempList2->nextListItem != NULL;
            tempList2=tempList2->nextListItem){
            for(int i = 0; i < 4; i++){
                critDist =  (tempList2->subtype.s.position[i].x-asteroidList->subtype.a.com.pos.x) *
                            (tempList2->subtype.s.position[i].x-asteroidList->subtype.a.com.pos.x) +
                            (tempList2->subtype.s.position[i].y-asteroidList->subtype.a.com.pos.y) *
                            (tempList2->subtype.s.position[i].y-asteroidList->subtype.a.com.pos.y);
                if(critDist<=(0+asteroidList->subtype.a.size)*(0+asteroidList->subtype.a.size)){
                    tempList2->subtype.s.com.life = 0;
                    asteroidList->subtype.a.com.life = 0;
                    if(asteroidList->subtype.a.size == LARGE){
                        PopulateAsteroidList(asteroidList, 3, MEDIUM,
                                             asteroidList->subtype.a.com.pos.x,
                                             asteroidList->subtype.a.com.pos.y);
                        break;
                    } else if(asteroidList->subtype.a.size == MEDIUM){
                        PopulateAsteroidList(asteroidList, 3, TINY,
                                             asteroidList->subtype.a.com.pos.x,
                                             asteroidList->subtype.a.com.pos.y);
                        break;
                    }
                }
            }
        }
    }
}

void GarbageCollect(EntityLists *entityList){
    Entity *asteroidList = &(entityList->firstAsSentinel);

    for(asteroidList=asteroidList->nextListItem;
        asteroidList->nextListItem != NULL;
        asteroidList=asteroidList->nextListItem){
        if(!(asteroidList->subtype.a.com.life)){
            asteroidList->nextListItem->prevListItem = asteroidList->prevListItem;
            asteroidList->prevListItem->nextListItem = asteroidList->nextListItem;
            free(asteroidList);
        }
    }
    Entity *projectileList = &(entityList->firstPrSentinel);

    for(projectileList=projectileList->nextListItem;
        projectileList->nextListItem != NULL;
        projectileList=projectileList->nextListItem){
        if(!(projectileList->subtype.p.com.life)){
            projectileList->nextListItem->prevListItem = projectileList->prevListItem;
            projectileList->prevListItem->nextListItem = projectileList->nextListItem;
            free(projectileList);
        }

    }
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    for(spaceShipList=spaceShipList->nextListItem;
        spaceShipList->nextListItem != NULL;
        spaceShipList=spaceShipList->nextListItem){
        if(!(spaceShipList->subtype.s.com.life)){
            spaceShipList->nextListItem->prevListItem = spaceShipList->prevListItem;
            spaceShipList->prevListItem->nextListItem = spaceShipList->nextListItem;
            free(spaceShipList);
        }
    }
}

void ChangeVelocites(Entity *spaceShipList){
    for(spaceShipList=spaceShipList->nextListItem;
        spaceShipList->nextListItem != NULL;
        spaceShipList=spaceShipList->nextListItem){
            spaceShipList->subtype.s.com.vel.x += cos(spaceShipList->subtype.s.com.orient)*spaceShipList->subtype.s.com.vel.acc;
            spaceShipList->subtype.s.com.vel.y += sin(spaceShipList->subtype.s.com.orient)*spaceShipList->subtype.s.com.vel.acc;
    }
}

void ManageEntities(EntityLists *entityList, Context *context){
    MoveEntities(entityList);
    DetectCollision(entityList, context);
    GarbageCollect(entityList);
}

void GetRotatedSpaceshipPoints(Entity *spaceShipList, Sint16 *posX, Sint16 *posY){
    posX[0] = spaceShipList->subtype.s.com.pos.x + 10*cos(spaceShipList->subtype.s.com.orient + 0);
    posY[0] = spaceShipList->subtype.s.com.pos.y + 10*sin(spaceShipList->subtype.s.com.orient + 0);

    posX[1] = spaceShipList->subtype.s.com.pos.x + 10*cos(spaceShipList->subtype.s.com.orient + 150*(PI/180));
    posY[1] = spaceShipList->subtype.s.com.pos.y + 10*sin(spaceShipList->subtype.s.com.orient + 150*(PI/180));

    posX[2] = spaceShipList->subtype.s.com.pos.x + 2*cos(spaceShipList->subtype.s.com.orient + 180*(PI/180));
    posY[2] = spaceShipList->subtype.s.com.pos.y + 2*sin(spaceShipList->subtype.s.com.orient + 180*(PI/180));

    posX[3] = spaceShipList->subtype.s.com.pos.x + 10*cos(spaceShipList->subtype.s.com.orient - 150*(PI/180));
    posY[3] = spaceShipList->subtype.s.com.pos.y + 10*sin(spaceShipList->subtype.s.com.orient - 150*(PI/180));
    for(int i = 0; i < 4; i++){
        spaceShipList->subtype.s.position[i].x = posX[i];
        spaceShipList->subtype.s.position[i].y = posY[i];
    }
}

void DrawSpaceShip(Entity *spaceShipList){
    for(spaceShipList=spaceShipList->nextListItem;
        spaceShipList->nextListItem != NULL;
        spaceShipList=spaceShipList->nextListItem){
        if(spaceShipList->subtype.s.com.life){
            if(spaceShipList->type == SPACESHIP){
                Sint16 x[4];
                Sint16 y[4];
                GetRotatedSpaceshipPoints(spaceShipList, &x, &y);

                aapolygonRGBA(screen,
                              &x,
                              &y,
                              4,
                              255, 255, 255, 255);

//              TODO collision triangle
#ifdef DEBUG
                circleRGBA(screen,
                           spaceShipList->subtype.s.com.pos.x,
                           spaceShipList->subtype.s.com.pos.y,
                           10, 255, 0, 0, 255);
#endif
            }
        }
    }
}

void GetAsteroidPoints(Entity *asteroidList, Sint16 *posX, Sint16 *posY, int n){
    for(int i = 0; i < n; i++){
        posX[i] = (asteroidList->subtype.a.size/5) * asteroidList->subtype.a.surfacJag[i] +
        asteroidList->subtype.a.com.pos.x +
        asteroidList->subtype.a.size * cos(((360/n)*(i+1))*(PI/180));
        posY[i] = (asteroidList->subtype.a.size/5) * asteroidList->subtype.a.surfacJag[i] +
        asteroidList->subtype.a.com.pos.y +
        asteroidList->subtype.a.size * sin(((360/n)*(i+1))*(PI/180));
    }
}

void DrawAsteroid(Entity *asteroidList){
    int ap = AP;
    Sint16 x[ap];
    Sint16 y[ap];

    for(asteroidList=asteroidList->nextListItem;
        asteroidList->nextListItem != NULL;
        asteroidList=asteroidList->nextListItem){
        if(asteroidList->subtype.a.com.life){
            if(asteroidList->type == ASTEROID){
                GetAsteroidPoints(asteroidList, &x, &y, ap);

                aapolygonRGBA(screen,
                              &x,
                              &y,
                              ap,
                              255, 255, 255, 255);
#ifdef DEBUG
//              collision circle
                circleRGBA(screen,
                            asteroidList->subtype.a.com.pos.x,
                            asteroidList->subtype.a.com.pos.y,
                            asteroidList->subtype.a.size,
                            255, 0, 0, 255);
#endif
            }
        }
    }
}

void DrawProjectile(Entity *projectileList){
    for(projectileList=projectileList->nextListItem;
        projectileList->nextListItem != NULL;
        projectileList=projectileList->nextListItem){
        if(projectileList->subtype.p.com.life){
            if(projectileList->type == PROJECTILE){
            circleRGBA(screen,
                       projectileList->subtype.a.com.pos.x,
                       projectileList->subtype.a.com.pos.y,
                       1, 255, 255, 255, 255);
            }
        }
        projectileList->subtype.p.com.life -= 1;
    }
}

void ReDraw(EntityLists *entityList, Context *context){
    Entity *asteroidList = &(entityList->firstAsSentinel);
    Entity *projectileList = &(entityList->firstPrSentinel);
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    SDL_FillRect(screen,0,0);

    if (font) {
        SDL_Surface *score;
        SDL_Color white = {255, 255, 255};
        char points[20];

        sprintf(points, "%d", context->points);
        score = TTF_RenderUTF8_Solid(font, points, white);
        SDL_BlitSurface(score, NULL, screen, NULL);
        SDL_FreeSurface(score);
    }

    DrawSpaceShip(spaceShipList);
    DrawAsteroid(asteroidList);
    DrawProjectile(projectileList);
    SDL_Flip(screen);
}

void RunGame(EntityLists *entityList, Context *context){
    GameLoop(entityList, context);
}

void InputHandler(EntityLists *entityList, Context *context){
    Entity *asteroidList = &(entityList->firstAsSentinel);
    Entity *projectileList = &(entityList->firstPrSentinel);
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    Uint8 *keyState = SDL_GetKeyState(NULL);
    if(keyState[SDLK_RIGHT]){
//        entityList->entityListPtr[0].orient += 0.05;
        Entity *tempList= spaceShipList;
        for(tempList=tempList->nextListItem;
            tempList->nextListItem != NULL;
            tempList=tempList->nextListItem){
                tempList->subtype.s.com.orient += 0.05;
        }
    }
    if(keyState[SDLK_LEFT]){
//        entityList->entityListPtr[0].orient -= 0.05;
        Entity *tempList= spaceShipList;
        for(tempList=tempList->nextListItem;
            tempList->nextListItem != NULL;
            tempList=tempList->nextListItem){
                tempList->subtype.s.com.orient -= 0.05;
        }
    }

    //if(keyState[SDLK_SPACE]){
//        entityList->entityListPtr[0].orient -= 0.05;
    //    Shoot(projectileList, spaceShipList);
    //}

    if(keyState[SDLK_UP]){
        ChangeVelocites(spaceShipList);
    }

    while(SDL_PollEvent(&event)){
        switch(event.type){
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym){
            case SDLK_ESCAPE:
                exit(0);
                break;

            case SDLK_SPACE:
                Shoot(projectileList, spaceShipList);
                break;

            default:
                break;
            }
        break;

        case SDL_QUIT:
            exit (0);
            break;
        }
    }
}

void GameLoop(EntityLists *entityList, Context *context){
    Entity *asteroidList = &(entityList->firstAsSentinel);
    Entity *projectileList = &(entityList->firstPrSentinel);
    Entity *spaceShipList = &(entityList->firstSsSentinel);

    unsigned int delay;
    unsigned int start;
    while(true){
        start = SDL_GetTicks();

        InputHandler(entityList, context);
        ManageEntities(entityList, context);
        ReDraw(entityList, context);

        //the mighty 60fps
        delay = start + 16 - (unsigned int)SDL_GetTicks();
        if(delay < 50){
            SDL_Delay(delay);
        }
        if(isListEmpty(asteroidList)){
            context->level++;
            context->points += context->level*10;
            PopulateAsteroidList(asteroidList, context->level, LARGE, 0, 0);
        }
    }
}

bool isListEmpty(Entity *list){
    if(list->prevListItem == list->nextListItem->nextListItem) return true;
    return false;
}

int main(int argc, char *argv[]) {

    EntityLists entityList;

    entityList.firstAsSentinel.prevListItem = NULL;
    entityList.firstAsSentinel.nextListItem = &(entityList.lastAsSentinel);

    entityList.lastAsSentinel.nextListItem = NULL;
    entityList.lastAsSentinel.prevListItem = &(entityList.firstAsSentinel);

    entityList.firstPrSentinel.prevListItem = NULL;
    entityList.firstPrSentinel.nextListItem = &(entityList.lastPrSentinel);

    entityList.lastPrSentinel.nextListItem = NULL;
    entityList.lastPrSentinel.prevListItem = &(entityList.firstPrSentinel);

    entityList.firstSsSentinel.prevListItem = NULL;
    entityList.firstSsSentinel.nextListItem = &(entityList.lastSsSentinel);

    entityList.lastSsSentinel.nextListItem = NULL;
    entityList.lastSsSentinel.prevListItem = &(entityList.firstSsSentinel);

    Context context;
    context.level = 1;
    context.points = 0;

    Init(&entityList, &context);

    if (font){
        TTF_CloseFont(font);
    }

    return 0;
}
